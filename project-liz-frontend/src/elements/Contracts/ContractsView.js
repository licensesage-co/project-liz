import './Contracts.css';
import CustomerContract from './CustomerContract';

function ContractsView() {
   return (
      <div className="contracts-container">
         <CustomerContract />
         <CustomerContract />
         <CustomerContract />
      </div>
   );
}

export default ContractsView;
