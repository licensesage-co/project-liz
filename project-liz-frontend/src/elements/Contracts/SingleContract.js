import './Contracts.css';

function SingleContract() {
   return (
      <div>
         <div className="contracts single-element">
            <div></div>
            <div>2022-08-17</div>
            <div>2024-08-17</div>
            <div>6.12</div>
            <div className="buttons-contract">
               <div className="buttons orange">
                  <a className="iconfont">&#xe817;</a>
               </div>
               <div className="buttons red">
                  <a className="iconfont">&#xe81b;</a>
               </div>
            </div>
         </div>
      </div>
   );
}

export default SingleContract;
