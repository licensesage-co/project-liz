import './Contracts.css';
import CustomSingleElement from '../Customers/CustomerSingleElement';
import SingleContract from './SingleContract';

function CustomerContract() {
   return (
      <div className="contracts">
         <div>Customer A</div>
         <div>Contract start</div>
         <div>Contract end</div>
         <div>Version</div>
         <div></div>
         <div className="contract-elements">
            <SingleContract />
            <SingleContract />
         </div>
      </div>
   );
}

export default CustomerContract;
