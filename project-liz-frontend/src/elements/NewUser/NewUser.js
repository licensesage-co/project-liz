import './NewUser.css';

function NewUser() {
   return (
      <div className="new-user-panel">
         <div className="create-user-panel">
            <div className="create-user-header">
               <a>Edit User</a>
            </div>

            <div>
               <form className="custom-form">
                  <a>Customer</a>
                  <select>
                     <option>Company A</option>
                     <option>Company B</option>
                     <option>Company C</option>
                  </select>

                  <a>First name</a>
                  <input />

                  <a>Last name</a>
                  <input />

                  <a>E-mail</a>
                  <input />

                  <a>Phone</a>
                  <input />

                  <a>Mobile</a>
                  <input />

                  <a>Is administrator</a>
                  <input className="checkboxalign" type="checkbox" />

                  <div className="footerbuttons">
                     <button className="new-user-button save-button">Save</button>
                     <button className="new-user-button cancel-button">Cancel</button>
                  </div>
               </form>
            </div>

            <div className="footer">

            </div>
         </div>
      </div>
   );
}

export default NewUser;
