import './Topbar.css';

function TopBar() {
   return (
      <div className="topbarcontainer">
         <div className="header">
            Customers
         </div>
         <div className="right">
            <div className="searchbar">
               <input className="searchinput" placeholder="Customer"></input>
               <a className="iconfont searchicon">&#xe825;</a>
            </div>
            <div>
               <a className="iconfont icons">&#xF2BE;</a>
            </div>
            <div>
               <a className="iconfont icons">&#xE824;</a>
            </div>
         </div>
      </div>
   );
}

export default TopBar;
