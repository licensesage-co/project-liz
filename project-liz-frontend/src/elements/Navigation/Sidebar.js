import './Sidebar.css';

function Sidebar() {
   return (
      <div className="sidebarcontainer">
         <div className="topspacer"></div>
         <div className="sidebarcontent">
            <div className="contentspaces">
               <a className="iconfont iconspace">&#xe806;</a>
               <a>Customers</a>
            </div>
         </div>
         <div className="sidebarcontent">
            <div className="contentspaces">
               <a className="iconfont iconspace">&#xe807;</a>
               <a>Contracts</a>
            </div>
         </div>
         <div className="sidebarcontent">
            <div className="contentspaces">
               <a className="iconfont iconspace">&#xf1b3;</a>
               <a>Instances</a>
            </div>
         </div>
         <div className="sidebarcontent">
            <div className="contentspaces">
               <a className="iconfont iconspace">&#xe806;</a>
               <a>Users</a>
            </div>
         </div>
      </div>
   );
}

export default Sidebar;
