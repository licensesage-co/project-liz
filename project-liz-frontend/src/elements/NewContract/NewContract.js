import './NewContract.css';

function NewContract() {
   return (
      <div className="new-user-panel">
         <div className="create-user-panel">
            <div className="create-user-header">
               <a>New Contract</a>
            </div>

            <div>
               <form className="custom-form-contract">
                  <a>Start</a>
                  <input type="date" />

                  <a>End</a>
                  <input type="date" />

                  <a>Version</a>
                  <select>
                     <option>5.3</option>
                     <option>6.1</option>
                     <option>7.2</option>
                     <option>7.6</option>
                  </select>

                  <a>Responsible</a>
                  <input />

                  <a>IP number</a>
                  <input />

                  <a>Feature A</a>
                  <input />

                  <a>IP number</a>
                  <input />

                  <a>Feature B</a>
                  <input />

                  <a>IP number</a>
                  <input />

                  <a>Feature C</a>
                  <input />

                  <a>License key</a>
                  <textarea className="cont-area" />
                  <div className="footerbuttons-cont">
                     <button className="new-user-button-cont save-button">Update key</button>
                     <button className="new-user-button-cont save-button">Mail key</button>
                     <div className="margin-div"></div>
                     <button className="new-user-button-cont save-button">Save</button>
                     <button className="new-user-button-cont cancel-button">Cancel</button>
                  </div>
               </form>
            </div>
            <div className="footer">

            </div>
         </div>
      </div>
   );
}

export default NewContract;
