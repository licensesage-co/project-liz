import './AddInstance.css';

function AddInstance() {
   return (
      <div className="new-customer-panel">
         <div className="create-user-panel">
            <div className="create-user-header">
               <a>Add instance</a>
            </div>

            <div>
               <form className="custom-form-newcustomer">
                  <a>Customer</a>
                  <select>
                     <option>Customer A</option>
                     <option>Customer B</option>
                     <option>Customer C</option>
                  </select>

                  <a>Instance name</a>
                  <input />

                  <a>Contract</a>
                  <select>
                     <option>Contract A</option>
                     <option>Contract B</option>
                     <option>Contract C</option>
                  </select>

                  <a>IP address</a>
                  <input />

                  <a>Type</a>
                  <select>
                     <option>A</option>
                     <option>B</option>
                     <option>C</option>
                  </select>

                  <div className="footerbuttons">
                     <button className="new-user-button save-button">Save</button>
                     <button className="new-user-button cancel-button">Cancel</button>
                  </div>
               </form>
            </div>

            <div className="footer">

            </div>
         </div>
      </div>
   );
}

export default AddInstance;
