import './CustomerUserCustomerView.css';

function CustomerUserCustomerView() {
	return (
		<div className="usercontainer">
			<div className="username">
				<label>User1</label>
			</div>
			<div className="useremail">
				<label>User1@web.de</label>
			</div>
			<a className="iconfont">&#xe817;</a>
			<a className="iconfont">&#xe81b;</a>
		</div>
	);
}

export default CustomerUserCustomerView;
