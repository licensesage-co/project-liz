import './CustomerUserView.css';
import CustomerUserCustomerView from './CustomerUserCustomerView';

function CustomerUserView() {
	return (
		<div className="customercontainer">
			<div className="customername">
				<a>Customer A</a>
			</div>
			<div className="users">
				<CustomerUserCustomerView />
				<CustomerUserCustomerView />
				<CustomerUserCustomerView />
			</div>
		</div>
	);
}

export default CustomerUserView;
