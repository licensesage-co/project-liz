import SingleInstance from "./SingleInstance";


function Instances() {
   return (
      <div className="contracts">
         <div>Customer A</div>
         <div></div>
         <div></div>
         <div></div>
         <div></div>
         <div className="contract-elements">
            <SingleInstance />
            <SingleInstance />
            <SingleInstance />
            <SingleInstance />
         </div>
      </div>
   );
}

export default Instances;
