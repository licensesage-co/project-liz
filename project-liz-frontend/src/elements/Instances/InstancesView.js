import Instances from './Instances';

function InstancesView() {
   return (
      <div className="contracts-container">
         <Instances />
         <Instances />
      </div>
   );
}

export default InstancesView;
