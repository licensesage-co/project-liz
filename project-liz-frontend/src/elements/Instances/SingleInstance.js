import './Instance.css';

function SingleInstance() {
   return (
      <div className="singlecontainer">
         <div className="flex">
            <div className="textcenter"></div>
         </div>
         <div className="flex">
            <div className="textcenter">
               Instace AA
            </div>
         </div>
         <div className="flex">
            <div className="textcenter">
               2022-08-17
            </div>
         </div>
         <div className="flex">
            <div className="textcenter">
               2024-08-17
            </div>
         </div>
         <div className="flex iconscustomer">
            <div className="customicon orange">
               <a className="iconfont">&#xe817;</a>
            </div>
            <div className="customicon red">
               <a className="iconfont">&#xe81b;</a>
            </div>
         </div>
      </div>
   );
}

export default SingleInstance;
