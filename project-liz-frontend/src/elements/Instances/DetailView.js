import './DetailView.css';

function DetailView() {
	return (
		<div className="new-customer-panel">
			<div className="create-user-panel">
				<div className="create-user-header">
					<a>Details</a>
				</div>

				<div>
					<form className="custom-form-newcustomer">
						<a>Service Contract Start</a>
						<input type="Date" />

						<a>Service Contract End</a>
						<input type="Date" />

						<a>Instance Name</a>
						<input />

						<a>IP address</a>
						<input />

						<a>Type</a>
						<input />

						<a>Status</a>
						<input />

						<div className="footerbuttons">
							<button className="cancel-button">EXIT</button>
						</div>
					</form>
				</div>

				<div className="footer" />
			</div>
		</div>
	);
}

export default DetailView;
