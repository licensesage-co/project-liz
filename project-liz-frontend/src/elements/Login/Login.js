import './Login.css';

function Login() {
	return (
		<div className="wrapper">
			<h1>Project Liz</h1>
			<div className="Login">
				<label className="login-label">Login</label>
				<input type="E-mail" placeholder="youremail@web.de" id="E-mail" name="E-mail" />
				<label className="emaillabel" for="E-mail">
					E-mail
				</label>
				<input type="Password" placeholder="*****" id="Password" name="Password" />
				<label for="Password">Password</label>
				<button>Login</button>
			</div>
		</div>
	);
}

export default Login;
