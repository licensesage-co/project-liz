import './Customer.css';

function CustomSingleElement() {
   return (
      <div className="singlecontainer">
         <div className="flex">
            <div className="textcenter">
               Customer A
            </div>
         </div>
         <div className="flex">
            <div className="textcenter">
               Address details A
            </div>
         </div>
         <div className="flex">
            <div className="textcenter">
               Address details B
            </div>
         </div>
         <div className="flex iconscustomer">
            <div className="customicon orange">
               <a className="iconfont">&#xe817;</a>
            </div>
            <div className="customicon red">
               <a className="iconfont">&#xe81b;</a>
            </div>
            <div className="customicon grey">
               <a className="iconfont">&#xe807;</a>
            </div>
            <div className="customicon darkgrey">
               <a className="iconfont">&#xe805;</a>
            </div>
         </div>
      </div>
   );
}

export default CustomSingleElement;
