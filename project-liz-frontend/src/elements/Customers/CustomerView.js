import './Customer.css';
import CustomSingleElement from './CustomerSingleElement';

function CustomerView() {
   return (
      <div className="container">
         <CustomSingleElement />
         <CustomSingleElement />
         <CustomSingleElement />
         <CustomSingleElement />
         <CustomSingleElement />
         <CustomSingleElement />
      </div>
   );
}

export default CustomerView;
