import './NewCustomer.css';

function NewCustomer() {
   return (
      <div className="new-customer-panel">
         <div className="create-user-panel">
            <div className="create-user-header">
               <a>Add customer</a>
            </div>

            <div>
               <form className="custom-form-newcustomer">
                  <a>Customer name</a>
                  <input />

                  <a>Address details A</a>
                  <input />

                  <a>Address details B</a>
                  <input />

                  <a>Phone</a>
                  <input />

                  <a>Mobile</a>
                  <input />

                  <div className="footerbuttons">
                     <button className="new-user-button save-button">Save</button>
                     <button className="new-user-button cancel-button">Cancel</button>
                  </div>
               </form>
            </div>

            <div className="footer">

            </div>
         </div>
      </div>
   );
}

export default NewCustomer;
