import './App.css';
import Login from './elements/Login/Login';
import SideBar from './elements/Navigation/Sidebar';
import TopBar from './elements/Navigation/TopBar';
import CustomerView from './elements/Customers/CustomerView';
import ContractsView from './elements/Contracts/ContractsView';
import InstancesView from './elements/Instances/InstancesView';
import Users from './elements/Users/Userview';
import NewUser from './elements/NewUser/NewUser';
import NewContract from './elements/NewContract/NewContract';
import NewCustomer from './elements/AddCustomer/NewCustomer';
import AddInstance from './elements/AddInstance/AddInstance';
import DetailView from './elements/Instances/DetailView';

//Hello
function App() {
	return (
		<div className="App">
			<TopBar />
			<SideBar />
			<div className="content" />
			<DetailView />
		</div>
	);
}

export default App;
