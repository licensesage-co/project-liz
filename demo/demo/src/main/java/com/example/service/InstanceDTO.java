package com.example.service;

public class InstanceDTO {
    private Long id;
    private String name;
    private String ipAddress;
    private String type;
    private Integer status;
    private Long contractId;

    // Constructors, getters, setters...
    // Default constructor
    public InstanceDTO() {
        
    }

    public InstanceDTO(Long id, String name, String ipAddress, String type, Integer status, Long contractId) {
        this.id = id;
        this.name = name;
        this.ipAddress = ipAddress;
        this.type = type;
        this.status = status;
        this.contractId = contractId;
    }

    // Getters and setters...
    public Long getId() {
        return id;
    }
    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }
    public void setName(String name) {
        this.name = name;
    }

    public String getIpAddress() {
        return ipAddress;
    }
    public void setIpAddress(String ipAddress) {
        this.ipAddress = ipAddress;
    }

     public String getType() {
        return type;
    }
    public void setType(String type) {
        this.type = type;
    }

     public Integer getStatus() {
        return status;
    }
    public void setStatus(Integer status) {
        this.status = status;
    }

    public Long getContractId() {
        return contractId;
    }
    public void setContractId(Long contractId) {
        this.contractId = contractId;
    }
}
