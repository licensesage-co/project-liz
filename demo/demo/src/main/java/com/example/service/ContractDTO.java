package com.example.service;

import java.util.Date;

import com.example.model.Contract;

public class ContractDTO {
    private Long id;
    private Date startDate;
    private Date endDate;
    private String ipAddress1;
    private String ipAddress2;
    private String ipAddress3;
    private String licenseKey;
    private Long user1Id;
    private Long user2Id;
    private int field1;
    private int field2;
    private int field3;
    private int field4;

    public ContractDTO(Contract contract){
    }
    
    // Constructors, getters, setters...
    public ContractDTO(Long id, Date startDate, Date endDate, String ipAddress1, String ipAddress2, String ipAddress3,
                       String licenseKey, Long user1Id, Long user2Id, int field1, int field2, int field3, int field4) {
        this.id = id;
        this.startDate = startDate;
        this.endDate = endDate;
        this.ipAddress1 = ipAddress1;
        this.ipAddress2 = ipAddress2;
        this.ipAddress3 = ipAddress3;
        this.licenseKey = licenseKey;
        this.user1Id = user1Id;
        this.user2Id = user2Id;
        this.field1 = field1;
        this.field2 = field2;
        this.field3 = field3;
        this.field4 = field4;
    }

    // Getters and setters...
    public Long getId() {
        return id;
    }
    public void setId(Long id) {
        this.id = id;
    }
    
    public Date getStartDate() {
        return startDate;
    }
    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

     public Date getEndDate() {
        return endDate;
    }
    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }

     public String getIpAddress1() {
        return ipAddress1;
    }
    public void setIpAddress1(String ipAddress1) {
        this.ipAddress1 = ipAddress1;
    }

     public String getIpAddress2() {
        return ipAddress2;
    }
    public void setIpAddress2(String ipAddress2) {
        this.ipAddress2 = ipAddress2;
    }

    public String getIpAddress3() {
        return ipAddress3;
    }
    public void setIpAddress3(String ipAddress3) {
        this.ipAddress3 = ipAddress3;
    }

    public String getLicenseKey() {
        return licenseKey;
    }
    public void setLicenseKey(String licenseKey) {
        this.licenseKey = licenseKey;
    }

     public Long getUser1Id() {
        return user1Id;
    }
    public void setUser1Id(Long user1Id) {
        this.user1Id = user1Id;
    }

     public Long getUser2Id() {
        return user2Id;
    }
    public void setUser2Id(Long user2Id) {
        this.user2Id = user2Id;
    }

    public int getField1() {
        return field1;
    }
    public void setField1(int field1) {
        this.field1 = field1;
    }

     public int getField2() {
        return field2;
    }
    public void setField2(int field2) {
        this.field2 = field2;
    }

    public int getField3() {
        return field3;
    }
    public void setField3(int field3) {
        this.field3 = field3;
    }

     public int getField4() {
        return field4;
    }
    public void setField4(int field4) {
        this.field4 = field4;
    }
}

