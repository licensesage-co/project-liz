package com.example.service;

import com.example.model.Customer;


public class CustomerDTO {

    private Long id;
    private String name;
    private String department;
    private String street;
    private String town;
    private String zipcode;
    private String country;
    
    public CustomerDTO(){
    }

    public CustomerDTO(long id, String name, String department, String street, String town, String zipcode, String country){
        this.id = id;
        this.name = name;
        this.department = department;
        this.street = street;
        this.town = town;
        this.zipcode = zipcode;
        this.country = country;
        
    }
    
    // Getters and setters
     public String getName() {
        return name;
    }
    public void setName(String name) {
        this.name = name;
    } 

    public String getDepartment() {
        return department;
    }
    public void setDepartment(String department) {
        this.department = department;
    }

    public String getStreet() {
        return street;
    }
    public void setStreet(String street) {
        this.street = street;
    }

    public String getTown() {
        return town;
    }
    public void setTown(String town) {
        this.town = town;
    }

    public String getZipcode() {
        return zipcode;
    }
    public void setZipcode(String zipcode) {
        this.zipcode = zipcode;
    }

    public String getCountry() {
        return country;
    }
    public void setCountry(String country) {
        this.country = country;
    }

    public Long getId(Long id){
        return id;
    }
    public void setId(Long id) {
        this.id = id;
    }
   
}
