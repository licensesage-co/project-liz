
package com.example.service;

public class RegisterDTO {

    private String loginName;
    private String email;
    private String password;

    // Default constructor
    public RegisterDTO() {
    }

    // Parameterized constructor
    public RegisterDTO(String loginName, String email, String password) {
        this.loginName = loginName;
        this.email = email;
        this.password = password;
    }

    // Getters and setters for each field
    public String getLoginName() {
        return loginName;
    }
    public void setLoginName(String username) {
        this.loginName = username;
    }

    public String getEmail() {
        return email;
    }
    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }
    public void setPassword(String password) {
        this.password = password;
    }
}
