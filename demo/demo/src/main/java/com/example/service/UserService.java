package com.example.service;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import com.example.model.User;
import com.example.repository.UserRepository;

@Service
public class UserService implements UserDetailsService {

    private final UserRepository userRepository;
    private final PasswordEncoder passwordEncoder;

    @Autowired
    public UserService(UserRepository userRepository, PasswordEncoder passwordEncoder) {
        this.userRepository = userRepository;
        this.passwordEncoder = passwordEncoder;
    }

    public User registerUser(RegisterDTO registerRequest) {
         // Perform registration logic, e.g., validate input, create a new user entity, etc.

         User newUser = new User();
         newUser.setLoginName(registerRequest.getLoginName());
         newUser.setEmail(registerRequest.getEmail());
         newUser.setPassword(passwordEncoder.encode(registerRequest.getPassword()));
         // Set other user properties as needed

         return userRepository.save(newUser);
    }

    // Implementing UserDetailsService method
    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        // Implement the logic to load user details fFrom the database
        User user = userRepository.findByLoginName(username)
                .orElseThrow(() -> new UsernameNotFoundException("User not found with username: " + username));

        // Convert the User entity to UserDetails
        return org.springframework.security.core.userdetails.User.builder()
                .username(user.getLoginName())
                .password(user.getPassword())
                .roles("USER") // You can set roles based on your application logic
                .build();
    }

    public Optional<User> findByLoginName(String loginName) {
        return userRepository.findByLoginName(loginName);
    }

    public boolean existsByLoginName(String loginName) {
        return userRepository.existsByLoginName(loginName);
    }

    public boolean existsByEmail(String email) {
        return userRepository.existsByEmail(email);
    }

    public User save(User user) {
        // Optionally, you can encode the password before saving
        user.setPassword(passwordEncoder.encode(user.getPassword()));

        // Add any additional logic before saving, if needed

        return userRepository.save(user);
    }

    // Add more methods as needed for user-related operations
}
