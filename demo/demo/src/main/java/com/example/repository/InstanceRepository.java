package com.example.repository;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.example.model.Instance;

public interface InstanceRepository extends JpaRepository<Instance, Long> {
    // CRUD methods
    Optional<Instance> findById(Long id);
    List<Instance> findAll();
    <S extends Instance> S save(S entity);

    <S extends Instance> S saveAndFlush(S Instance);

    void deleteById(Long id);

    @Modifying
    @Query("UPDATE Instance i SET i.name = :newName WHERE i.id = :id")
    void updateName(@Param("id") Long instanceId, @Param("newName") String newName);

    @Modifying
    @Query("UPDATE Instance i SET i.status = :newStatus WHERE i.id = :id")
    void updateStatus(@Param("id") Long instanceId, @Param("newStatus") String newStatus);

    @Modifying
    @Query("UPDATE Instance i SET i.ipAddress = :newipAddress WHERE i.id = :id")
    void updateIpAddress(@Param("id") Long instanceId, @Param("newipAddress") String newipAddress);
}
