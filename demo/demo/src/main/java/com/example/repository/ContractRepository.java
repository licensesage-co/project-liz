package com.example.repository;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.example.model.Contract;

public interface ContractRepository extends JpaRepository<Contract, Long> {
    // CRUD methods
    Optional<Contract> findById(Long id);
    List<Contract> findAll();
    <S extends Contract> S save(S entity);
   
    <S extends Contract> S saveAndFlush(S Contract);
    void deleteById(Long id);

    
    @Modifying
    @Query("UPDATE Contract c SET c.startDate = :newStartDate WHERE c.id = :id")
    void updateStartDate(@Param("id") Long ContractId, @Param("newStartDate") String newStartDate);

    @Modifying
    @Query("UPDATE Contract c SET c.endDate = :newEndDate WHERE c.id = :id")
    void updateEndDate(@Param("id") Long ContractId, @Param("newEndDate") String newEndDate);

    @Modifying
    @Query("UPDATE Contract c SET c.ipAddress1 = :newipAddress1 WHERE c.id = :id")
    void updateIpAdress1(@Param("id") Long ContractId, @Param("newipAddress1") String newIpAdress1);

    @Modifying
    @Query("UPDATE Contract c SET c.ipAddress2 = :newipAddress2 WHERE c.id = :id")
    void updateIpAdress2(@Param("id") Long ContractId, @Param("newipAddress2") String newIpAdress2);

    @Modifying
    @Query("UPDATE Contract c SET c.ipAddress3 = :newipAddress3 WHERE c.id = :id")
    void updateIpAdress3(@Param("id") Long ContractId, @Param("newipAddress3") String newIpAdress3);

    @Modifying
    @Query("UPDATE Contract c SET c.user1 = :newUser1 WHERE c.id = :id")
    void updateUser1Id(@Param("id") Long ContractId, @Param("newUser1") String newUser1);

    @Modifying
    @Query("UPDATE Contract c SET c.user2 = :newUser2 WHERE c.id = :id")
    void updateUser2Id(@Param("id") Long ContractId, @Param("newUser2") String newUser2);


}   
