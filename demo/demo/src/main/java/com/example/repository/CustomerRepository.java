package com.example.repository;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.example.model.Customer;


public interface CustomerRepository extends JpaRepository<Customer, Long> {
    //Crud Methods
    Optional<Customer> findById(Long id);
    List<Customer> findAll();
    <S extends Customer> S save(S entity);
    
    <S extends Customer> S saveAndFlush(S Customer);
    void deleteById(Long id);
    //basic queries
    // Custom query method to update the street for a customer
    @Modifying
    @Query("UPDATE Customer c SET c.street = :newStreet WHERE c.id = :id")
    void updateStreet(@Param("id") Long customerId, @Param("newStreet") String newStreet);

    @Modifying
    @Query("UPDATE Customer c SET c.department = :newDepartment WHERE c.id = :id")
    void updateDepartment(@Param("id") Long customerId, @Param("newDepartment") String newDepartment);

    @Modifying
    @Query("UPDATE Customer c SET c.country = :newCountry WHERE c.id = :id")
    void updateCountry(@Param("id") Long customerId, @Param("newCountry") String newCountry);

    @Modifying
    @Query("UPDATE Customer c SET c.town = :newTown WHERE c.id = :id")
    void updateTown(@Param("id") Long customerId, @Param("newTown") String newTown);
}
