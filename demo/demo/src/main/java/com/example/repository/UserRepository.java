package com.example.repository;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.example.model.User;

public interface UserRepository extends JpaRepository<User, Long> {
    // CRUD methods    
    Optional<User> findById(Long id);
    List<User> findAll();
    <S extends User> S save(S entity);
    Optional<User> findByLoginName(String loginName);
    // Check if a user with the given username exists
    boolean existsByLoginName(String loginName);
    // Check if a user with the given email exists
    boolean existsByEmail(String email);
    
     @Modifying
    @Query("UPDATE User u SET u.firstName = :newFirstName WHERE u.id = :userId")
    void updateFirstName(@Param("userId") Long userId, @Param("newFirstName") String newFirstName);

    @Modifying
    @Query("UPDATE User u SET u.lastName = :newLastName WHERE u.id = :userId")
    void updateLastName(@Param("userId") Long userId, @Param("newLastName") String newLastName);

    @Modifying
    @Query("UPDATE User u SET u.email = :newEmail WHERE u.id = :userId")
    void updateEmail(@Param("userId") Long userId, @Param("newEmail") String newEmail);

    @Modifying
    @Query("UPDATE User u SET u.phoneNumber1 = :newPhone1 WHERE u.id = :userId")
    void updatePhone1(@Param("userId") Long userId, @Param("newPhone1") String newPhone1);

    @Modifying
    @Query("UPDATE User u SET u.phoneNumber2 = :newPhone2 WHERE u.id = :userId")
    void updatePhone2(@Param("userId") Long userId, @Param("newPhone2") String newPhone2);

    @Modifying
    @Query("UPDATE User u SET u.loginName = :newLoginName WHERE u.id = :userId")
    void updateLoginName(@Param("userId") Long userId, @Param("newLoginName") String newLoginName);

    @Modifying
    @Query("UPDATE User u SET u.password = :newPassword WHERE u.id = :userId")
    void updatePassword(@Param("userId") Long userId, @Param("newPassword") String newPassword);

    @Modifying
    @Query("UPDATE User u SET u.isAdmin = :newAdmin WHERE u.id = :userId")
    void updateAdmin(@Param("userId") Long userId, @Param("newAdmin") String newAdmin);
}


