package com.example.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.model.User;
import com.example.repository.UserRepository;

import io.swagger.v3.oas.annotations.Operation;

@RestController
@RequestMapping("/api/users")
public class UserController {
    // Implement the required functions
    private final UserRepository userRepository;

    @Autowired
    public UserController(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    @PostMapping("/CreateUser")
    public void createUser(@RequestBody User user, String loginName) {      
        if(userRepository.existsByLoginName(loginName)){
            user.setLoginName(loginName);
            user.setPassword(user.getPassword());
            user.setEmail(user.getEmail());
            System.out.print("Guy already exists");
            userRepository.save(user);
        }else{
            user.setId(null);
            userRepository.save(user);
        }
    }

    public void setLoginName(@RequestBody User user){
        userRepository.updateLoginName(user.getId(), null);
    }

    @GetMapping("/api/GetUsers}")
    public List<User> getAllUser() {
        return userRepository.findAll();
    }

    @DeleteMapping("/api/DeleteUser/{id}")
    @Operation(summary = "Delete a user by ID")
    public void deleteUser(@PathVariable long id) {
        // Delete the user in the database
        userRepository.deleteById(id);
    }
}

