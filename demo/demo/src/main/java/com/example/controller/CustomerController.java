package com.example.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.model.Customer;
import com.example.repository.CustomerRepository;

import io.swagger.v3.oas.annotations.Operation;

@RestController
@RequestMapping("/api/customers")
public class CustomerController {
    // Implement the required functions
    private final CustomerRepository customerRepository;

    @Autowired
    public CustomerController(CustomerRepository customerRepository) {
        this.customerRepository = customerRepository;
    }
    
    @PostMapping("/CreateCustomer")
    public Customer createCustomer(@RequestBody Customer customer) {
        // Ensure the ID is null before saving to let the database generate it
        customer.setId(null);

        // Save the customer to the database
        return customerRepository.save(customer);
    }

    @GetMapping("/api/GetCustomers}")
    public List<Customer> getAllCustomers() {
        return customerRepository.findAll();
    }

    @DeleteMapping("/api/DeleteCustomer/{id}")
    @Operation(summary = "Delete a customer by ID")
    public void deleteCustomer(@PathVariable long id) {
        // Delete the customer in the database
        customerRepository.deleteById(id);
    }
}

