package com.example.controller;

import java.util.List;

import javax.persistence.EntityNotFoundException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.model.Contract;
import com.example.repository.ContractRepository;
import com.example.repository.UserRepository;
import com.example.model.User;
import io.swagger.v3.oas.annotations.Operation;

@RestController
@RequestMapping("/api/contracts")
public class ContractController {
    // Implement the required functions
    private final ContractRepository contractRepository;
    @Autowired
    private UserRepository userRepository;
    @Autowired
    public ContractController(ContractRepository ContractRepository) {
        this.contractRepository = ContractRepository;
    }

    @PostMapping("/CreateContract")
    public Contract createContract(@RequestBody Contract contract, Long user1Id, Long user2Id) {
        
        User user1 = userRepository.findById(user1Id).orElseThrow(() -> new EntityNotFoundException("User1 not found with id: " + user1Id));
        User user2 = userRepository.findById(user2Id).orElseThrow(() -> new EntityNotFoundException("User2 not found with id: " + user2Id));
       
        contract.setUser1(user1);
        contract.setUser2(user2);
        
        return contractRepository.save(contract);
    }

    @GetMapping("/api/GetContracts}")
    public List<Contract> getAllContracts() {
        return contractRepository.findAll();
    }

    @DeleteMapping("/api/DeleteContract/{id}")
    @Operation(summary = "Delete a Contract by ID")
    public void deleteContract(@PathVariable long id) {
        // Delete the Contract in the database
        contractRepository.deleteById(id);
    }
}

