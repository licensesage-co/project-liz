package com.example.controller;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.example.model.User;
import com.example.service.LoginDTO;
import com.example.service.RegisterDTO;
import com.example.service.UserService;

@RestController
public class AuthController {

    private final UserService userService;
    private final PasswordEncoder passwordEncoder;

    @Autowired
    public AuthController(UserService userService, PasswordEncoder passwordEncoder) {
        this.userService = userService;
        this.passwordEncoder = passwordEncoder;
    }

    @PostMapping("/custom-login")
    public ResponseEntity<String> login(@RequestBody LoginDTO loginForm) {
        Optional<User> userOptional = userService.findByLoginName(loginForm.getLoginName());
    
        if (userOptional.isPresent()) {
            User user = userOptional.get();
    
            if (passwordEncoder.matches(loginForm.getPassword(), user.getPassword())) {
                return ResponseEntity.ok("Login successful!");
            }
        }
    
        return ResponseEntity.status(HttpStatus.UNAUTHORIZED).body("Invalid username or password");
    }

    @PostMapping("/register")
    public ResponseEntity<User> registerUser(@RequestBody RegisterDTO registerRequest) {
        User newUser = userService.registerUser(registerRequest);
        return new ResponseEntity<>(newUser, HttpStatus.CREATED);
    }
}

