package com.example.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api")
public class APIController {

   @GetMapping("/greet/{name}") //PostMapping is for creating a nice guy, get for getting
    public String greet(@PathVariable String name) {
        return "Hello, " + name + "!";
    }
    // Add more methods for other endpoints as needed
}
