package com.example.controller;

import java.util.List;

import javax.persistence.EntityNotFoundException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.model.Contract;
import com.example.model.Instance;
import com.example.model.User;
import com.example.repository.ContractRepository;
import com.example.repository.InstanceRepository;
import com.example.service.LoginDTO;

import io.swagger.v3.oas.annotations.Operation;

@RestController
@RequestMapping("/api/instance")
public class InstanceController {
    // Implement the required functions
    private final InstanceRepository instanceRepository;
    @Autowired
    private ContractRepository contractRepository;

    @Autowired
    public InstanceController(InstanceRepository instanceRepository) {
        this.instanceRepository = instanceRepository;
    }
    
    @PostMapping("/CreateInstance")
    public Instance saveInstance(@RequestBody Instance instance, Long contractId) {
        // You might want to perform additional logic or validation before saving
        Contract contract = contractRepository.findById(contractId).orElseThrow(() -> new EntityNotFoundException("Contract not found with id: " + contractId));
        
        // Set the retrieved Contract in the Instance entity
        instance.setContract(contract);
        // Save the Instance entity
        return instanceRepository.save(instance);
    }
     
    @GetMapping("/api/GetInstances}")
    public List<Instance> getAllInstances() {
        return instanceRepository.findAll();
    }

    @DeleteMapping("/api/DeleteInstance/{id}")
    @Operation(summary = "Delete a Instance by ID")
    public ResponseEntity<String> deleteInstance(@PathVariable long id) {
        if(contractRepository.findById(id) != null){
            return ResponseEntity.status(HttpStatus.CONFLICT).body("Instance cannot be deleted, Contract still exists");
        } 
        instanceRepository.deleteById(id);
        return ResponseEntity.ok("Succesfully deleted Instance");
    }
}

