package com.example.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "customer")

public class Customer {
    public Customer(){

    }
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(length = 60, name = "name")
    private String name;

    @Column(length = 30, name = "department")
    private String department;

    @Column(length = 60, name = "street")
    private String street;

    @Column(length = 60, name = "town")
    private String town;

    @Column(length = 10, name = "zip_code")
    private String zipCode;

    @Column(length = 30, name = "country")
    private String country;

    // Getters and setters, constructors, etc.
     public Long getId() {
        return id;
    }
    public void setId(Long id) {
        this.id = id;
    }

     public String getName() {
        return name;
    }
    public void setName(String name) {
        this.name = name;
    }

     public String getDepartment() {
        return department;
    }
    public void setDepartment(String department) {
        this.department = department;
    }
    
    public String getStreet() {
        return street;
    }
    public void setStreet(String street) {
        this.street = street;
    }

     public String getTown() {
        return town;
    }
    public void setTown(String town) {
        this.town = town;
    }

     public String getZipCode() {
        return zipCode;
    }
    public void setZipCode(String zipCode) {
        this.zipCode = zipCode;
    }

    public String getCountry() {
        return country;
    }
    public void setCountry(String country) {
        this.country = country;
    }

    
}
