package com.example.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name = "contract")
public class Contract {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    public Long getId() {
        return id;
    }
    public void setId(Long id) {
        this.id = id;
    }

    @Temporal(TemporalType.DATE)
    @Column(name = "start_date")
    private Date startDate;
    public Date getStartDate() {
        return startDate;
    }
    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    @Temporal(TemporalType.DATE)
    @Column(name = "end_date")
    private Date endDate;
    public Date getEndDate() {
        return endDate;
    }
    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }

    @Column(length = 20, name = "ip_address1")
    private String ipAddress1;
    public String getIpAddress1() {
        return ipAddress1;
    }
    public void setIpAddress1(String ipAddress1) {
        this.ipAddress1 = ipAddress1;
    }

    @Column(length = 20, name = "ip_address2")
    private String ipAddress2;
    public String getIpAddress2() {
        return ipAddress2;
    }
    public void setIpAddress2(String ipAddress2) {
        this.ipAddress2 = ipAddress2;
    }

    @Column(length = 20, name = "ip_address3")
    private String ipAddress3;
    public String getIpAddress3() {
        return ipAddress3;
    }
    public void setIpAddress3(String ipAddress3) {
        this.ipAddress3 = ipAddress3;
    }

    @Lob
    @Column(name = "license_key")
    private String licenseKey;
    public String getLicenseKey() {
        return licenseKey;
    }
    public void setLicenseKey(String licenseKey) {
        this.licenseKey = licenseKey;
    }

    @ManyToOne
    @JoinColumn(name = "user1_id")
    private User user1;
    public User getUser1() {
        return user1;
    }
    public void setUser1(User user1) {
        this.user1 = user1;
    }

    @ManyToOne
    @JoinColumn(name = "user2_id")
    private User user2;
    public User getUser2() {
        return user2;
    }
    public void setUser2(User user2) {
        this.user2 = user2;
    }

    @Column(name = "field1")
    private int integerField1;
    public int getIntegerField1() {
        return integerField1;
    }
    public void setIntegerField1(int integerField1) {
        this.integerField1 = integerField1;
    }

    @Column(name = "field2")
    private int integerField2;
    public int getIntegerField2() {
        return integerField2;
    }
    public void setIntegerField2(int integerField2) {
        this.integerField2 = integerField2;
    }

    @Column(name = "field3")
    private int integerField3;
    public int getIntegerField3() {
        return integerField3;
    }
    public void setIntegerField3(int integerField3) {
        this.integerField3 = integerField3;
    }
    @Column(name = "field4")
    private int integerField4;
    public int getIntegerField4() {
        return integerField4;
    }
    public void setIntegerField4(int intergerField4) {
        this.integerField4 = intergerField4;
    }

    // Getters and setters, constructors, etc.
}
