package com.example.config;

import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;

@Configuration
@EnableWebSecurity
public class SecurityConfig extends WebSecurityConfigurerAdapter {

    private final SecurityBeansConfig securityBeansConfig;

    public SecurityConfig(SecurityBeansConfig securityBeansConfig) {
        this.securityBeansConfig = securityBeansConfig;
    }

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http
                .authorizeRequests(requests -> requests
                        .antMatchers("/**").permitAll() // Allow access to all pages
                        .anyRequest().authenticated())
                .formLogin(login -> login
                        .loginProcessingUrl("/custom-login-endpoint") // Set a different processing URL
                        .loginPage("/login")
                        .successHandler(securityBeansConfig.successHandler())  
                        .permitAll())
                .logout(logout -> logout
                        .permitAll())
                .csrf(csrf -> csrf.disable());
    }
    
}
