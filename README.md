<h1>Project-Liz</h1>

<a>About the project:</a> <br/>
This is a project for esslingen university / software architecture.
The goal is a license management system with a SpringBoot REST API, POSTGRESQL database system and ReactJS Frontend.

<h2>LicenseManager</h2>

<b>Project by:<br/></b>
Florian Schneider<br/>
Maxim Matiks<br/>
Joey Kiss<br/>
Dung Minh Ho<br/>
